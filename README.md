# Пример Map-Reduce задачи для подсчета слов в тексте. #

 * Компиляция:

`mvn package`

 * Запуск:

```
hadoop jar mr-wordcount-1.0-SNAPSHOT.jar ru.mai.dep806.bigdata.mr.WordCount /user/stud/textdata /user/stud/output
```